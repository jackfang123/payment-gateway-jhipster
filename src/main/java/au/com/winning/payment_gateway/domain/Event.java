package au.com.winning.payment_gateway.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnore;

import au.com.winning.payment_gateway.domain.enumeration.NotificationType;

/**
 * A Event.
 */
@Entity
@Table(name = "event")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Event implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotNull
	@Type(type = "org.hibernate.type.ZonedDateTimeType")
	@Column(name = "created", nullable = false)
	private ZonedDateTime created;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "notification_type", nullable = false)
	private NotificationType notificationType;

	@NotNull
	@Size(min = 1)
	@Column(name = "payload", nullable = false, columnDefinition = "json")
	private String payload;

	@OneToMany(mappedBy = "event", cascade = CascadeType.ALL)
	@JsonIgnore
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private List<Notification> notifications = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ZonedDateTime getCreated() {
		return created;
	}

	public Event created(ZonedDateTime created) {
		this.created = created;
		return this;
	}

	public void setCreated(ZonedDateTime created) {
		this.created = created;
	}

	public NotificationType getNotificationType() {
		return notificationType;
	}

	public Event notificationType(NotificationType notificationType) {
		this.notificationType = notificationType;
		return this;
	}

	public void setNotificationType(NotificationType notificationType) {
		this.notificationType = notificationType;
	}

	public String getPayload() {
		return payload;
	}

	public Event payload(String payload) {
		this.payload = payload;
		return this;
	}

	public void setPayload(String payload) {
		this.payload = payload;
	}

	public List<Notification> getNotifications() {
		return notifications;
	}

	public Event notifications(List<Notification> notifications) {
		this.notifications = notifications;
		return this;
	}

	public Event addNotifications(Notification notification) {
		notifications.add(notification);
		notification.setEvent(this);
		return this;
	}

	public Event removeNotifications(Notification notification) {
		notifications.remove(notification);
		notification.setEvent(null);
		return this;
	}

	public void setNotifications(List<Notification> notifications) {
		this.notifications = notifications;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Event event = (Event) o;
		if (event.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, event.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "Event{" + "id=" + id + ", created='" + created + "'" + ", notificationType='" + notificationType + "'" + ", payload='" + payload + "'" + '}';
	}
}
