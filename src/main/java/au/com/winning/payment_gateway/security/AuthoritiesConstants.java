package au.com.winning.payment_gateway.security;

import liquibase.util.StringUtils;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

	public static final String ADMIN = "ROLE_ADMIN";

	public static final String USER = "ROLE_USER";

	public static final String ANONYMOUS = "ROLE_ANONYMOUS";

	public static final String NOTIFICATION = "ROLE_NOTIFICATION";

	public static final String REFUND = "ROLE_REFUND";

	public static final String PAYMENT = "ROLE_PAYMENT";

	private AuthoritiesConstants() {
	}

	public static String asRole(String authority) {
		if (StringUtils.startsWith(authority, "ROLE_") == false) {
			throw new IllegalArgumentException(String.format("expected authority %s to begin with ROLE_", authority));
		}
		return authority.replaceFirst("ROLE_", "");
	}
}
