package au.com.winning.payment_gateway.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import au.com.winning.payment_gateway.service.WebSocketClientService;
import au.com.winning.pos_dto.PedLibraryCallbackDto;
import au.com.winning.pos_dto.TenderResponseDto;

@RestController
@RequestMapping(PedController.PATH)
public class PedController {
	private Logger logger = LoggerFactory.getLogger(PedController.class);

	public static final String PATH = "/ped";

	@Autowired
	private WebSocketClientService webSocketClientService;

	@PostMapping("/libraryCallback/{id}")
	public void libraryCallback(@RequestBody PedLibraryCallbackDto request, @PathVariable String id) {
		logger.info("ped library callback {}", request);
		webSocketClientService.pedLibraryCallback(id, request);
	}

	@PostMapping("/libraryFinalCallback/{id}")
	public void libraryFinalCallback(@RequestBody TenderResponseDto dto, @PathVariable String id) {
		logger.info("got ped library final callback {}", dto);
		webSocketClientService.completeTender(id, dto);
	}
}
