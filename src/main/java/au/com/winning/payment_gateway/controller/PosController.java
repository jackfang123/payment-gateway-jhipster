package au.com.winning.payment_gateway.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import au.com.winning.payment_gateway.controller.dto.TenderRequestDto;
import au.com.winning.payment_gateway.controller.dto.assembler.TenderRequestDtoAssembler;
import au.com.winning.payment_gateway.service.PedService;

@RestController
@RequestMapping(PosController.PATH)
public class PosController {
	private Logger logger = LoggerFactory.getLogger(PosController.class);

	public static final String PATH = "/pos";
	@Autowired
	private PedService pedService;

	private TenderRequestDtoAssembler assembler = new TenderRequestDtoAssembler();

	@PostMapping("/createTender/{id}")
	public void createTender(@RequestBody TenderRequestDto request, @PathVariable String id) {
		logger.info("got {} from {}", request, id);

		au.com.winning.pos_dto.TenderRequestDto posRequest = assembler.assemble(request, "WinningPOS");
		logger.debug("sending to pos agent {}", posRequest);
		pedService.createTender(posRequest, id);

	}

}
