package au.com.winning.payment_gateway.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import au.com.winning.payment_gateway.controller.dto.ValidationErrorDto;

@ControllerAdvice
public class DefaultExceptionHandler {
	private Logger logger = LoggerFactory.getLogger(DefaultExceptionHandler.class);

	@Autowired
	private MessageSource messageSource;

	@ExceptionHandler(InvalidRequestException.class)
	public ResponseEntity<List<ValidationErrorDto>> handleInvalidRequestException(InvalidRequestException ex) {
		logger.warn(ex.toString());

		List<ValidationErrorDto> errors = new ArrayList<>();
		for (FieldError fieldError : ex.getResult().getFieldErrors()) {
			ValidationErrorDto errorDto = new ValidationErrorDto();
			errorDto.setField(fieldError.getField());
			errorDto.setValue(fieldError.getRejectedValue());
			errorDto.setReason(messageSource.getMessage(fieldError, Locale.getDefault()));

			errors.add(errorDto);
		}

		return ResponseEntity.badRequest().body(errors);
	}

	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<String> handleConstraintViolationException(ConstraintViolationException ex) {
		logger.warn(ex.getConstraintViolations().toString(), ex);
		return ResponseEntity.badRequest().body(ex.getConstraintViolations().toString());
	}
}
