package au.com.winning.payment_gateway.controller.dto;

import java.math.BigDecimal;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class PaymentFormDto {
	@Size(min = 1)
	@NotNull
	// doesn't allow the word false, can be returned by the adyen encryption function in case of errors during encryption
	@Pattern(regexp = "^(?!false).*$")
	private String encryptedAdyenData;

	@DecimalMin(inclusive = false, value = "0")
	@NotNull
	private BigDecimal amount;

	public String getEncryptedAdyenData() {
		return encryptedAdyenData;
	}

	public void setEncryptedAdyenData(String encryptedAdyenData) {
		this.encryptedAdyenData = encryptedAdyenData;
	}

	@Override
	public String toString() {
		return "PaymentFormDto [encryptedAdyenData=" + encryptedAdyenData + ", amount=" + amount + "]";
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
}
