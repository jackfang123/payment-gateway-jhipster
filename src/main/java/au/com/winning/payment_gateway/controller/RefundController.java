package au.com.winning.payment_gateway.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.adyen.services.payment.Refund;
import com.adyen.services.payment.RefundResponse;

import au.com.winning.adyen_client.service.dto.RefundRequest;
import au.com.winning.adyen_client.service.raw.RefundService;
import au.com.winning.payment_gateway.service.MerchantAccountService;

@Controller
@RequestMapping(RefundController.PATH)
public class RefundController {
	private Logger logger = LoggerFactory.getLogger(RefundController.class);

	public static final String PATH = "/refund";

	@Autowired
	private RefundService rawRefundService;

	@Autowired
	private au.com.winning.adyen_client.service.RefundService refundService;

	@Autowired
	private MerchantAccountService merchantAccountService;

	@PostMapping(value = "/raw/{company}", consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public RefundResponse submitRawRefund(@RequestBody Refund request, @PathVariable String company, @RequestParam(required = false) String subdivision) {
		return rawRefundService.refund(request);
	}

	@PostMapping(value = "/{company}", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public au.com.winning.adyen_client.service.dto.RefundResponse submitRefund(@RequestBody RefundRequest request, @PathVariable String company,
			@RequestParam(required = false) String subdivision) {

		logger.info(request.toString());

		au.com.winning.adyen_client.service.dto.RefundResponse response = refundService.refund(request,
				merchantAccountService.getMerchantAccount(company, subdivision));
		logger.info(response.toString());
		return response;
	}
}
