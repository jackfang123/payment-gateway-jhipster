package au.com.winning.payment_gateway.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.com.winning.payment_gateway.assembler.EventAssembler;
import au.com.winning.payment_gateway.domain.Event;
import au.com.winning.payment_gateway.domain.enumeration.NotificationType;
import au.com.winning.payment_gateway.repository.EventRepository;

@Controller
@RequestMapping(NotificationController.PATH)
public class NotificationController {
	public static final String PATH = "/notification";

	private Logger logger = LoggerFactory.getLogger(NotificationController.class);

	@Autowired
	private EventAssembler assembler;

	@Autowired
	private EventRepository eventDao;

	@RequestMapping(value = "/{type}", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<String> receive(@RequestBody String json, @PathVariable NotificationType type) {
		logger.info(json);

		Event event = assembler.assemble(type, json);
		eventDao.save(event);
		return ResponseEntity.ok("[accepted]");
	}
}
