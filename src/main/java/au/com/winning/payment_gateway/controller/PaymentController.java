package au.com.winning.payment_gateway.controller;

import java.util.UUID;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.adyen.services.payment.Authorise;
import com.adyen.services.payment.AuthoriseResponse;

import au.com.winning.adyen_client.service.PaymentService;
import au.com.winning.adyen_client.service.dto.PaymentRequest;
import au.com.winning.adyen_client.service.dto.PaymentResponse;
import au.com.winning.adyen_client.service.dto.PaymentWithRecurringSetupRequest;
import au.com.winning.adyen_client.service.dto.RecurringPaymentRequest;
import au.com.winning.adyen_client.service.dto.fraud.Address;
import au.com.winning.adyen_client.service.dto.fraud.Browser;
import au.com.winning.adyen_client.service.dto.fraud.Contact;
import au.com.winning.adyen_client.service.dto.fraud.FraudCheckingDetails;
import au.com.winning.adyen_client.service.dto.fraud.Line;
import au.com.winning.payment_gateway.controller.dto.PaymentFormDto;
import au.com.winning.payment_gateway.service.MerchantAccountService;
import au.com.winning.random_utils.RandomUtils;

@Controller
@RequestMapping(PaymentController.PATH)
public class PaymentController {
	public static final String PATH = "/payment";

	private Logger logger = LoggerFactory.getLogger(PaymentController.class);

	@Autowired
	private PaymentService paymentService;

	@Autowired
	private au.com.winning.adyen_client.service.raw.PaymentService rawPaymentService;

	@Autowired
	private MerchantAccountService merchantAccountService;

	@Value("${adyen.merchantAccount}")
	private String merchantAccount;

	@PostMapping(consumes = { MediaType.APPLICATION_FORM_URLENCODED_VALUE })
	public String submit(@Valid PaymentFormDto form, BindingResult result) {
		if (result.hasErrors()) {
			logger.error(result.toString());
			return "redirect:/static/index.html";
		}

		logger.debug(form.toString());

		PaymentRequest request = new PaymentRequest();
		request.setReferenceNo(UUID.randomUUID().toString());
		request.setAmount(form.getAmount());
		request.setEncryptedAdyenDetails(form.getEncryptedAdyenData());

		request.setFraudCheckingDetails(createFraudCheckingDetails());

		PaymentResponse response = paymentService.pay(request, merchantAccount);
		logger.info(response.toString());
		return "redirect:/static/index.html";
	}

	@PostMapping(path = "/json", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseBody
	public ResponseEntity submitJson(@RequestBody @Valid PaymentFormDto form, BindingResult result) {
		if (result.hasErrors()) {
			logger.error(result.toString());
			return ResponseEntity.badRequest().body(result.toString());
		}

		logger.debug(form.toString());

		PaymentRequest request = new PaymentRequest();
		request.setReferenceNo(UUID.randomUUID().toString());
		request.setAmount(form.getAmount());
		request.setEncryptedAdyenDetails(form.getEncryptedAdyenData());

		request.setFraudCheckingDetails(createFraudCheckingDetails());

		PaymentResponse response = paymentService.pay(request, merchantAccount);
		logger.info(response.toString());
		return ResponseEntity.ok(response);
	}

	private FraudCheckingDetails createFraudCheckingDetails() {
		FraudCheckingDetails det = RandomUtils.createInstance(FraudCheckingDetails.class);
		det.setBillingContact(createContact());
		det.setShippingContact(createContact());
		det.setBrowser(RandomUtils.createInstance(Browser.class));
		det.getBrowser().setIp("127.0.0.1");
		det.getLines().add(RandomUtils.createInstance(Line.class));
		return det;
	}

	private Contact createContact() {
		Contact contact = RandomUtils.createInstance(Contact.class);
		contact.setAddress(RandomUtils.createInstance(Address.class));
		contact.getAddress().setPostcode("2000");
		contact.getAddress().setCountry("AU");
		contact.setEmail(RandomUtils.createEmail());
		return contact;
	}

	@PostMapping(value = "/raw", consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public AuthoriseResponse submitRawPayment(@RequestBody Authorise request) {
		return rawPaymentService.pay(request);
	}

	@PostMapping(value = "/new/{company}", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public PaymentResponse submitNewPayment(@RequestBody @Valid PaymentRequest request, BindingResult result, @PathVariable String company,
			@RequestParam(required = false) String subdivision) {

		if (result.hasErrors()) {
			throw new InvalidRequestException(result);
		}

		logger.info(request.toString());

		PaymentResponse response = paymentService.pay(request, merchantAccountService.getMerchantAccount(company, subdivision));
		logger.info(response.toString());
		return response;
	}

	@PostMapping(value = "/newWithRecurringSetup/{company}", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public PaymentResponse submitNewPaymentWithRecurringSetup(@RequestBody @Valid PaymentWithRecurringSetupRequest request, BindingResult result,
			@PathVariable String company, @RequestParam(required = false) String subdivision) {

		if (result.hasErrors()) {
			throw new InvalidRequestException(result);
		}
		logger.info(request.toString());
		PaymentResponse response = paymentService.payWithRecurringSetup(request, merchantAccountService.getMerchantAccount(company, subdivision));

		logger.info(response.toString());
		return response;
	}

	@PostMapping(value = "/recurring/{company}", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public PaymentResponse submitRecurringPayment(@RequestBody @Valid RecurringPaymentRequest request, BindingResult result, @PathVariable String company,
			@RequestParam(required = false) String subdivision) {

		if (result.hasErrors()) {
			throw new InvalidRequestException(result);
		}

		logger.info(request.toString());
		PaymentResponse response = paymentService.payRecurring(request, merchantAccountService.getMerchantAccount(company, subdivision));
		logger.info(response.toString());
		return response;
	}
}
