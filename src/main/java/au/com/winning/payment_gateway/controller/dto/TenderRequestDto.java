package au.com.winning.payment_gateway.controller.dto;

import java.math.BigDecimal;

public class TenderRequestDto {
	private String currency;
	private BigDecimal amount;
	private String merchantReference;

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "TenderRequestDto [currency=" + currency + ", amount=" + amount + ", merchantReference=" + merchantReference + "]";
	}

	public String getMerchantReference() {
		return merchantReference;
	}

	public void setMerchantReference(String merchantReference) {
		this.merchantReference = merchantReference;
	}
}
