package au.com.winning.payment_gateway.controller.dto.assembler;

import au.com.winning.pos_dto.TenderRequestDto;

public class TenderRequestDtoAssembler {
	public TenderRequestDto assemble(au.com.winning.payment_gateway.controller.dto.TenderRequestDto dto, String merchantAccount) {
		TenderRequestDto ret = new TenderRequestDto();
		ret.setAmount(dto.getAmount());
		ret.setCurrency(dto.getCurrency());
		ret.setMerchantReference(dto.getMerchantReference());
		ret.setMerchantAccount(merchantAccount);
		return ret;
	}
}
