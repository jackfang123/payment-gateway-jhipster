package au.com.winning.payment_gateway.assembler;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;

import au.com.winning.payment_gateway.domain.Event;
import au.com.winning.payment_gateway.domain.Notification;
import au.com.winning.payment_gateway.domain.enumeration.NotificationType;

@Component
public class EventAssembler {

	@Autowired
	private ObjectMapper objectMapper;

	public Event assemble(NotificationType type, String json) {
		Event event = new Event();
		event.setNotificationType(type);
		event.setPayload(json);
		event.setCreated(ZonedDateTime.now());

		List<Map<String, Object>> notifications = JsonPath.parse(json).read("$.notificationItems[*].NotificationRequestItem");

		for (Map<String, Object> notification : notifications) {
			Notification not = new Notification();
			try {
				not.setJson(objectMapper.writeValueAsString(notification));
			} catch (JsonProcessingException e) {
				throw new RuntimeException("unable to write json", e);
			}
			event.addNotifications(not);
		}
		return event;
	}
}
