/**
 * View Models used by Spring MVC REST controllers.
 */
package au.com.winning.payment_gateway.web.rest.vm;
