/**
 * Data Access Objects used by WebSocket services.
 */
package au.com.winning.payment_gateway.web.websocket.dto;
