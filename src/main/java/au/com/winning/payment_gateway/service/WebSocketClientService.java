package au.com.winning.payment_gateway.service;

import au.com.winning.pos_dto.PedLibraryCallbackDto;
import au.com.winning.pos_dto.TenderResponseDto;

public interface WebSocketClientService {

	void pedLibraryCallback(String pedId, PedLibraryCallbackDto dto);

	void completeTender(String pedId, TenderResponseDto dto);
}
