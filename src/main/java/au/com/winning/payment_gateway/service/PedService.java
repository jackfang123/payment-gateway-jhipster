package au.com.winning.payment_gateway.service;

import au.com.winning.pos_dto.TenderRequestDto;

public interface PedService {

	void createTender(TenderRequestDto requestDto, String pedId);
}
