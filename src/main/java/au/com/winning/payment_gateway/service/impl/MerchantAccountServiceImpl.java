package au.com.winning.payment_gateway.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import au.com.winning.payment_gateway.service.MerchantAccountService;

@Service
public class MerchantAccountServiceImpl implements MerchantAccountService {

	@Autowired
	private Environment env;

	@Override
	public String getMerchantAccount(String company, String subdivision) {
		return env.getRequiredProperty("adyen.merchantAccount");
	}

}
