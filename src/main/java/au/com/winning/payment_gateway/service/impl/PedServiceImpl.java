package au.com.winning.payment_gateway.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import au.com.winning.payment_gateway.service.PedService;
import au.com.winning.pos_dto.TenderRequestDto;

@Service
public class PedServiceImpl implements PedService {

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public void createTender(TenderRequestDto requestDto, String pedId) {
		restTemplate.postForEntity("http://minhlin:8445/ped/createTender/{pedId}", requestDto, String.class, pedId);
	}

}
