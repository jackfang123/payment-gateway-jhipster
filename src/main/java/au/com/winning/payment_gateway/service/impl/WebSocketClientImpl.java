package au.com.winning.payment_gateway.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import au.com.winning.payment_gateway.service.WebSocketClientService;
import au.com.winning.pos_dto.PedLibraryCallbackDto;
import au.com.winning.pos_dto.TenderResponseDto;

@Service
public class WebSocketClientImpl implements WebSocketClientService {

	@Autowired
	private SimpMessagingTemplate template;

	@Override
	public void completeTender(String pedId, TenderResponseDto dto) {
		template.convertAndSend("/queue/createTenderResponse/" + pedId, dto);

	}

	@Override
	public void pedLibraryCallback(String pedId, PedLibraryCallbackDto dto) {
		template.convertAndSend("/queue/createTenderInProgress/" + pedId, dto);
	}

}
