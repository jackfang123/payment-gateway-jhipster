package au.com.winning.payment_gateway.service;

public interface MerchantAccountService {
	/**
	 * Retrieves the merchant account to use providing the company and optional subdivision
	 * 
	 * @param company
	 * @param subdivision
	 *            can be null
	 * @return
	 */
	String getMerchantAccount(String company, String subdivision);
}
