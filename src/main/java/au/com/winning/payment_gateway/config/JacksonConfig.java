package au.com.winning.payment_gateway.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@Configuration(value = "paymentGatewayJacksonConfig")
public class JacksonConfig {
	// @Bean
	// public Module dateTimeModule(ObjectMapper mapper) {
	// JavaTimeModule module = new JavaTimeModule();
	// mapper.registerModule(module);
	// return module;
	// }

	private Logger logger = LoggerFactory.getLogger(JacksonConfig.class);

	@Autowired
	public void customiseObjectMapper(@Qualifier("adyenClientObjectMapper") ObjectMapper mapper) {
		mapper.registerModule(new JavaTimeModule());
	}
	// @Bean
	// public MappingJackson2HttpMessageConverter jacksonMessageConverter(@Qualifier("adyenClientObjectMapper") ObjectMapper mapper)
	// throws JsonProcessingException {
	// mapper.registerModule(new JavaTimeModule());
	// logger.debug(mapper.writeValueAsString(DateTime.now()));
	// logger.debug(mapper.writeValueAsString(ZonedDateTime.now()));
	// return new MappingJackson2HttpMessageConverter(mapper);
	// }
}
