(function() {
    'use strict';

    angular
        .module('paymentGatewayApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
