(function() {
    'use strict';

    angular
        .module('testPaymentModule')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('test_payment', {
            parent: 'app',
            abstract: true
        });
    }
})();
