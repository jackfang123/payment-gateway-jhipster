(function() {
	'use strict';

	angular.module('testPaymentModule').controller('TerminalController', TerminalController);

	TerminalController.$inject = [ '$scope', 'Principal', '$state', '$q', '$log', 'TerminalPaymentService'];

	function TerminalController($scope, Principal, $state, $q, $log, TerminalPaymentService) {
		var vm = this;

		vm.stompClient = null;
		vm.tenderInProgress=false;
		vm.callbackMessages=null;
		vm.pay = {
			amount : "110",
			currency : "EUR",
			merchantReference: "test payment from payment gateway",
			pedId: "e355"
		}
		vm.createTender = function(pay) {
			$log.debug('making payment ' + pay.amount);
			vm.tenderInProgress=true;
			vm.callbackMessages=[];
			return TerminalPaymentService.createTender(pay.pedId, pay).then(function(mesg) {
				vm.callbackMessages.push(mesg);
				return mesg;
			},null, function(mesg) {
				vm.callbackMessages.push(mesg);
				return mesg;
			}).finally(function() {
				vm.tenderInProgress=false;
			}); 	
		}
	}
})();