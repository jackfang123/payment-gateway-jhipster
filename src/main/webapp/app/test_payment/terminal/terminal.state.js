(function() {
    'use strict';

    angular
        .module('testPaymentModule')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('terminal', {
            parent: 'test_payment',
            url: '/',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/test_payment/terminal/terminal.html',
                    controller: 'TerminalController',
                    controllerAs: 'vm'
                }
            }
        });
    }
})();
