(function() {
	'use strict';

	angular.module('testPaymentModule').controller('OnlineController', OnlineController);

	OnlineController.$inject = [ '$scope', 'Principal', '$state', 'OnlinePaymentService', '$q' ];

	function OnlineController($scope, Principal, $state, OnlinePaymentService, $q) {
		var vm = this;
		vm.paymentInProgress=false;
		vm.paymentResponse=null;
		
		vm.defaultPayDetails = {
			ccNumber : "4111111111111111",
			nameOnCard : "John Smith",
			expireMonth : "08",
			expireYear : "2018",
			cvc : "737",
			amount : "100",
			generationTime : new Date().toISOString()
		}

		vm.submitPayment = function(pay) {
			vm.paymentInProgress=true;
			vm.paymentResponse=null;
			var encryptedData = OnlinePaymentService.encryptPayment(pay);
			return OnlinePaymentService.submitPayment(encryptedData, pay.amount).then(function(response) {
				vm.paymentResponse=response;
			}, function(response) {
				vm.paymentResponse=response;
				return $q.reject();
			}).finally(function() {
				vm.paymentInProgress=false;
			});
		}

		vm.resetPayment = function() {
			vm.pay = angular.copy(vm.defaultPayDetails);
		}

		vm.resetPayment();
	}
})();