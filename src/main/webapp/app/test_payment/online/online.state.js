(function() {
    'use strict';

    angular
        .module('testPaymentModule')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('online', {
            parent: 'test_payment',
            url: '/',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/test_payment/online/online.html',
                    controller: 'OnlineController',
                    controllerAs: 'vm'
                }
            }
        });
    }
})();
