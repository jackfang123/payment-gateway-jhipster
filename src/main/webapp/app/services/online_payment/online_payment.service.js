(function() {
	'use strict';

	angular.module('testPaymentModule').factory('OnlinePaymentService', OnlinePaymentService);

	OnlinePaymentService.$inject = [ '$http', '$log', '$q' ];

	function OnlinePaymentService($http, $log, $q) {
		var key = "10001|C35589CE10E20944BE3CF08EE061B691DDC6EE8C5FC1AA29FFE51661E942894F43DE58D0CAE79C756EE804B64C23726B1001CC5024C9ACEA6D0F3B7E144204C7A0A0E560F00500FEE6EDB287242F8328DEC6EF275D4CD02181DEAB6A5CE62DA401F898BD504506E45D312D6C946A14B5DEFFA8531FEA4EBB60855F85D1DDDF5678BF4EF332632B0930E3B2AC56A04F8AF4C399CE71458E8131EAEF8FB326A1AE8C6F0978C6539376BDB3D29463136D8730BFB65F4DCBAAB7295D983D640BD7FE1B8EB8FC16F877DABD50CF337D318A272B21B8918D1045107426B95574345FA12EECC8BA23D8E7EBBFC806741DD1B8CB55050F665B910A14C31C80C802D96833";
		var options = {}; // See adyen.encrypt.nodom.html for details
		var service = {};

		service.cseInstance = adyen.encrypt.createEncryption(key, options);

		service.submitPayment = function(encryptedData, amount) {
			$log.debug("submitting payment " + amount);
			return $http.post('/payment/json', {
				encryptedAdyenData : encryptedData,
				amount : amount
			}).then(function(response) {
				return $q.resolve(response.data);
			});
		};

		service.encryptPayment = function(payDetails) {
			$log.debug("encrypting payment " + JSON.stringify(payDetails));

			var cardData = service.convertPayDetails(payDetails);

			$log.debug('validating ' + JSON.stringify(cardData));

			var valid = service.cseInstance.validate(cardData);
			$log.debug('validation result ' + JSON.stringify(valid));

			// need to set the generation time after validation
			cardData.generationtime = payDetails.generationTime;

			$log.debug('encrypting ' + JSON.stringify(cardData));
			return service.cseInstance.encrypt(cardData);
		}

		service.convertPayDetails = function(payDetails) {
			return {
				number : payDetails.ccNumber,
				cvc : payDetails.cvc,
				holderName : payDetails.nameOnCard,
				expiryMonth : payDetails.expireMonth,
				expiryYear : payDetails.expireYear,
			};
		}

		return service;
	}
})();
