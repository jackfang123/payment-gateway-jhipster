(function() {
	'use strict';

	angular.module('testPaymentModule').factory('TerminalPaymentService', TerminalPaymentService);

	TerminalPaymentService.$inject = [ '$log', '$q', '$window', '$http', '$cookies' ];

	function TerminalPaymentService($log, $q, $window, $http, $cookies) {
		var service = {};
		
		service.createTender = function(pedId, pay) {
			service.deferred=$q.defer();
			
			initWebsocket(pedId).then(function() {
				return $http.post("/pos/createTender/" + pedId, pay).then(function(response) {
					service.deferred.notify('submitted tender, waiting for a response');
					return response.data;
				}, function(data) {
					service.deferred.reject(data);
				});
				
			});
			
			
			return service.deferred.promise;
		}
		
		var stompClient=null;
		
		function initWebsocket(pedId) {
			var webSocketDeferred=$q.defer();
			
			var loc = $window.location;
            var url = '//' + loc.host + loc.pathname + 'websocket/terminal_payment';
		 	var socket = new SockJS(url);
            stompClient = Stomp.over(socket);
            var stateChangeStart;
            var headers = {};
            headers[$http.defaults.xsrfHeaderName] = $cookies.get($http.defaults.xsrfCookieName);
            stompClient.connect(headers, function() {
               $log.debug("websocket connected");
               
               stompClient.subscribe('/queue/createTenderResponse/' + pedId, function(mesg) {
            	  service.deferred.resolve(mesg.body); 
               });
               stompClient.subscribe('/queue/createTenderInProgress/' + pedId, function(mesg) {
            	  service.deferred.notify(mesg.body); 
               });
               
               webSocketDeferred.resolve();
            });
            
            return webSocketDeferred.promise;
		}
		
		return service;
	}
})();