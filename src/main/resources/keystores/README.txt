Sample command to import self signed cert on pos agent

sudo /usr/local/jdk/bin/keytool -importkeystore -srcstoretype pkcs12 -srckeystore keystores/minhmac.p12 -srcalias payment_gateway_dev -destkeystore /usr/local/jdk/jre/lib/security/cacerts -deststoretype jks -destalias payment_gateway_dev