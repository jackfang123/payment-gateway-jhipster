function createPayDetail() {
	return {
		ccNumber : "4111111111111111",
		nameOnCard : "John Smith",
		expireMonth: "08",
		expireYear: "2018",
		cvc: "737",
		amount: "100",
		generationTime: new Date().toISOString()
	}
}

function createTerminalPayDetail() {
	return {
		amount: "110",
		currency: "AUD",
		merchantReference: "test payment from payment gateway",
		pedId: "e355"
	}
}