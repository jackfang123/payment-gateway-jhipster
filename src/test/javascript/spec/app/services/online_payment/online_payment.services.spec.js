'use strict';

describe('Service Tests', function() {

	beforeEach(module('testPaymentModule', function($provide) {
		$provide.value('$log', console);
	}));

	describe('Payment', function() {
		var service;

		beforeEach(inject(function($injector) {
			service = $injector.get('OnlinePaymentService');

			expect(service.cseInstance).not.toBe(undefined);
		}));

		describe('non http', function() {
			it('should encrypt payment data', function() {
				var payDetail = createPayDetail();

				spyOn(service.cseInstance, 'encrypt').and.callThrough();

				expect(service.encryptPayment(payDetail)).not.toBe(undefined);

				expect(service.cseInstance.encrypt).toHaveBeenCalled();
			});
		});

		describe('http', function() {
			var $httpBackend;

			beforeEach(mockApiAccountCall);
			beforeEach(mockScriptsCalls);

			beforeEach(inject(function($injector) {
				$httpBackend = $injector.get('$httpBackend');
			}));

			afterEach(function() {
				$httpBackend.verifyNoOutstandingExpectation();
				$httpBackend.verifyNoOutstandingRequest();
			});

			describe('submit payment', function() {

				var encryptedData = "blah";
				var amount = "100.00";
				var response = { blah:true};
				
				it('success rest call', function() {
					
					
					$httpBackend.expectPOST('/payment/json', {
						amount : amount,
						encryptedAdyenData : encryptedData
					}).respond(response);
					
					var actualResponse=null;
					service.submitPayment(encryptedData, amount).then(function(response) {
						actualResponse=response;
					});
					expect(actualResponse).toBe(null);
					$httpBackend.flush();
					expect(actualResponse).toEqual(response);
				});
				
				it ('failed rest call', function() {
					$httpBackend.expectPOST('/payment/json', {
						amount: amount,
						encryptedAdyenData: encryptedData
					}).respond(400, response);
					
					var actualResponse;
					service.submitPayment(encryptedData, amount).then(function(response) {
						actualResponse=response.data;
					}).catch(function(response) {
						actualResponse="failed";
					});
					
					expect(actualResponse).toBeUndefined();
					$httpBackend.flush();
					expect(actualResponse).toBe("failed")
				});
			});
		});

	});
});
