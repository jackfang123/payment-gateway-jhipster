'use strict';

describe('Service Tests', function() {

	beforeEach(module('testPaymentModule', function($provide) {
		$provide.value('$log', console);
	}));
	
	describe('Terminal Payment', function() {
		
		var service;
		var $httpBackend;
		
		beforeEach(inject(function($injector) {
			service = $injector.get('TerminalPaymentService');
			$httpBackend = $injector.get('$httpBackend');
		}));
		
		beforeEach(mockApiAccountCall);
		beforeEach(mockScriptsCalls);
		
		afterEach(function() {
			$httpBackend.verifyNoOutstandingExpectation();
			$httpBackend.verifyNoOutstandingRequest();
		});
		
		xit('should create tender', function() {
			var pay = createTerminalPayDetail();
			var id = 'minh_ped';
			var success, notify;
			
			service.createTender(id, pay).then(function() {
				success=true;
			}, null, function() {
				notify=true;
			});
			
			$httpBackend.expectPOST('/pos/createTender/' + id, pay).respond();
			
			$httpBackend.flush();
			
			expect(success).toBeUndefined();
			expect(notify).toBe(true);
		});

	});
});
