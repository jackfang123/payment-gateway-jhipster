'use strict';

describe('Test Payment Online Tests', function() {

	beforeEach(function() {
		module('testPaymentModule', function($provide) {
			$provide.value('$log', console);
		});
	});
	
	beforeEach(mockApiAccountCall);
	beforeEach(mockScriptsCalls);

	var $scope, controller, $log, OnlinePaymentService, $q;

	beforeEach(inject(function($rootScope, $controller, _$log_, _OnlinePaymentService_, _$q_) {

		$scope = $rootScope.$new();
		controller = $controller('OnlineController as vm', {
			$scope : $scope
		});
		$log = _$log_;
		OnlinePaymentService = _OnlinePaymentService_;
		$q = _$q_;
	}));

	it('generation time to be set', function() {
		expect($scope.vm.pay.generationTime).not.toBe(undefined);
	});

	describe('reset payment', function() {
		it('should reset payment details on startup', function() {
			expect($scope.vm.pay).toEqual($scope.vm.defaultPayDetails);
		});
	});

	describe('submit payment', function() {
		it('success', function() {
			expect($scope.vm.paymentInProgress).toBe(false);
			var deferred = $q.defer();
			var encryptedData = "encryptedData";
			var response = "response";
			spyOn(OnlinePaymentService, 'encryptPayment').and.returnValue(encryptedData);
			spyOn(OnlinePaymentService, 'submitPayment').and.returnValue(deferred.promise);

			var payDetails = createPayDetail();

			var success;
			controller.submitPayment(payDetails).then(function() {
				success = true;
			}, function() {
				$log.error('some error occurred');
			});

			deferred.resolve(response);
			expect($scope.vm.paymentInProgress).toBe(true);
			expect(success).toBeUndefined();
			expect(OnlinePaymentService.encryptPayment).toHaveBeenCalledWith(payDetails);
			expect(OnlinePaymentService.submitPayment).toHaveBeenCalledWith(encryptedData, payDetails.amount);
			expect($scope.vm.paymentResponse).toBe(null);

			$scope.$apply();
			expect(success).toBe(true);
			expect($scope.vm.paymentInProgress).toBe(false);
			expect($scope.vm.paymentResponse).toBe(response);

			// should clear the result if we do another payment
			 controller.submitPayment(payDetails);
			 expect($scope.vm.paymentInProgress).toBe(true);
			 expect($scope.vm.paymentResponse).toBe(null);

		});

		it('fail', function() {
			expect($scope.vm.paymentInProgress).toBe(false);
			var deferred = $q.defer();
			var encryptedData = "encryptedData";
			var response = "response";
			spyOn(OnlinePaymentService, 'encryptPayment').and.returnValue(encryptedData);
			spyOn(OnlinePaymentService, 'submitPayment').and.returnValue(deferred.promise);

			var payDetails = createPayDetail();

			var success;
			controller.submitPayment(payDetails).then(function() {
				success = true;
			}, function() {
				success = false;
			});

			deferred.reject(response);
			expect($scope.vm.paymentInProgress).toBe(true);
			expect(success).toBeUndefined();
			expect($scope.vm.paymentResponse).toBe(null);
			expect(OnlinePaymentService.encryptPayment).toHaveBeenCalledWith(payDetails);
			expect(OnlinePaymentService.submitPayment).toHaveBeenCalledWith(encryptedData, payDetails.amount);

			$scope.$apply();
			expect(success).toBe(false);
			expect($scope.vm.paymentInProgress).toBe(false);
			expect($scope.vm.paymentResponse).toBe(response);
		});
	});

});
