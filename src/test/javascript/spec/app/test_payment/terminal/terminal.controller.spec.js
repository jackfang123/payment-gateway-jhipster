'use strict';

describe('Test Payment Terminal Tests', function() {

	beforeEach(function() {
		module('testPaymentModule', function($provide) {
			$provide.value('$log', console);
		});
	});
	
	beforeEach(mockApiAccountCall);
	beforeEach(mockScriptsCalls);

	var $scope, controller, $log, TerminalPaymentService, $q;

	beforeEach(inject(function($rootScope, $controller, _$log_, _TerminalPaymentService_, _$q_) {

		$scope = $rootScope.$new();
		controller = $controller('TerminalController as vm', {
			$scope : $scope
		});
		$log = _$log_;
		TerminalPaymentService = _TerminalPaymentService_;
		$q = _$q_;
	}));

	it('should submit payment', function() {
		var deferred=$q.defer();
		
		spyOn(TerminalPaymentService, 'createTender').and.returnValue(deferred.promise);
		
		var actualResponse, notifyResponse;

		expect($scope.vm.tenderInProgress).toBe(false);
		
		controller.createTender(createTerminalPayDetail()).then(function(response) {
			actualResponse=response;
			notifyResponse=null;
		}, null, function(response) {
			notifyResponse=response;
			actualResponse=null;
		});
		
		expect($scope.vm.tenderInProgress).toBe(true);
		expect(actualResponse).toBeUndefined();
		expect(notifyResponse).toBeUndefined();
		expect($scope.vm.callbackMessages).toEqual([]);
		
		deferred.notify("in progress");
		$scope.$apply();
		
		expect($scope.vm.tenderInProgress).toBe(true);
		expect(actualResponse).toBe(null);
		expect(notifyResponse).toBe("in progress");
		expect($scope.vm.callbackMessages).toEqual([notifyResponse]);
		
		deferred.resolve("end");
		$scope.$apply();

		expect($scope.vm.tenderInProgress).toBe(false);
		expect(actualResponse).toBe("end");
		expect(notifyResponse).toBe(null);
		expect($scope.vm.callbackMessages).toEqual(["in progress", actualResponse]);
	});
});
