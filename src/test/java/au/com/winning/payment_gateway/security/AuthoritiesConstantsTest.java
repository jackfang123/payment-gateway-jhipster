package au.com.winning.payment_gateway.security;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import au.com.winning.random_utils.RandomUtils;

public class AuthoritiesConstantsTest {
	@Test
	public void testAsRole() {
		String role = RandomUtils.createString();
		assertEquals(role, AuthoritiesConstants.asRole("ROLE_" + role));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAsRole_notRole() {
		AuthoritiesConstants.asRole(RandomUtils.createString());
	}
}
