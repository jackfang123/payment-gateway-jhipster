package au.com.winning.payment_gateway.service.impl;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import au.com.winning.payment_gateway.service.PedService;
import au.com.winning.pos_dto.TenderRequestDto;
import au.com.winning.pos_dto.utils.DtoFactory;
import au.com.winning.random_utils.RandomUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class PedServiceImplRestTest {

	@Configuration
	public static class Config {
		@Bean
		public RestTemplate restTemplate() {
			return new RestTemplate();
		}

		@Bean
		public PedService pedService() {
			return new PedServiceImpl();
		}
	}

	@Autowired
	private RestTemplate restTemplate;
	private MockRestServiceServer mockServer;

	@Autowired
	private PedService service;

	@Before
	public void beforeTest() {
		mockServer = MockRestServiceServer.bindTo(restTemplate).build();
	}

	@After
	public void afterTest() {
		mockServer.verify();
	}

	@Test
	public void testCreateTender() {
		String id = RandomUtils.createString();
		mockServer.expect(requestTo("http://localhost:8445/ped/createTender/" + id)).andExpect(method(HttpMethod.POST)).andRespond(withSuccess());

		TenderRequestDto requestDto = DtoFactory.createTenderRequestDto();
		service.createTender(requestDto, id);
	}
}
