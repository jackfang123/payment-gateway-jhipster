package au.com.winning.payment_gateway.controller;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.databind.ObjectMapper;

import au.com.winning.payment_gateway.service.WebSocketClientService;
import au.com.winning.payment_gateway.util.DtoFactory;
import au.com.winning.pos_dto.PedLibraryCallbackDto;
import au.com.winning.random_utils.RandomUtils;

public class PedControllerTest extends AbstractControllerTest {

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private WebSocketClientService webSocketClientService;

	@Test
	public void testLibraryCallback() throws Exception {
		String id = RandomUtils.createString();
		PedLibraryCallbackDto request = DtoFactory.createPedLibraryCallbackDto();
		// @formatter:off
		mockMvc.perform(post(PedController.PATH + "/libraryCallback/{id}", id)
				.secure(true)
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.content(objectMapper.writeValueAsString(request)))
			.andExpect(status().isOk());
		// @formatter:on

		verify(webSocketClientService).pedLibraryCallback(id, request);
	}
}
