package au.com.winning.payment_gateway.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.testSecurityContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.oxm.Marshaller;
import org.springframework.security.test.context.support.WithMockUser;

import com.adyen.services.payment.ModificationRequest;
import com.adyen.services.payment.Refund;
import com.adyen.services.payment.RefundResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

import au.com.winning.adyen_client.service.dto.RefundRequest;
import au.com.winning.adyen_client.service.raw.RefundService;
import au.com.winning.adyen_client.util.MarshallerUtil;
import au.com.winning.payment_gateway.service.MerchantAccountService;
import au.com.winning.random_utils.RandomUtils;

@WithMockUser(roles = "REFUND")
public class RefundControllerTest extends AbstractControllerTest {

	private String company;
	private String subdivision;

	@Autowired
	private MerchantAccountService merchantAccountService;

	@Autowired
	private RefundService rawRefundService;

	@Autowired
	private Marshaller marshaller;
	private String merchantAccount;

	@Autowired
	private ObjectMapper objectMapper;

	@Captor
	private ArgumentCaptor<Refund> rawRefundCaptor;

	@Autowired
	private au.com.winning.adyen_client.service.RefundService refundService;

	@Captor
	private ArgumentCaptor<RefundRequest> refundCaptor;

	@Before
	public void init() {
		company = RandomUtils.createString();
		subdivision = RandomUtils.createString();
		merchantAccount = RandomUtils.createString();
		reset(rawRefundService, merchantAccountService);
	}

	@Test
	public void testSubmitRawRefund_xml() throws Exception {
		Refund refund = new Refund();
		ModificationRequest modRequest = RandomUtils.createInstance(ModificationRequest.class);
		refund.setModificationRequest(modRequest);

		when(merchantAccountService.getMerchantAccount(company, subdivision)).thenReturn(merchantAccount);
		RefundResponse refundResponse = new RefundResponse();
		when(rawRefundService.refund(any(Refund.class))).thenReturn(refundResponse);

		// @formatter:off
		mockMvc.perform(post(RefundController.PATH + "/raw/" + company).
				secure(true).
				with(testSecurityContext()).
				content(MarshallerUtil.marshal(marshaller, refund)).
				param("subdivision", subdivision).
				contentType(MediaType.APPLICATION_XML).
				accept(MediaType.APPLICATION_XML)).
			andExpect(status().isOk()).
			andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_XML));
		// @formatter:on

		verify(rawRefundService).refund(rawRefundCaptor.capture());
		verifyZeroInteractions(merchantAccountService);
	}

	@Test
	public void testSubmitRawRefund_json() throws Exception {
		Refund refund = new Refund();
		ModificationRequest modRequest = RandomUtils.createInstance(ModificationRequest.class);
		refund.setModificationRequest(modRequest);

		when(merchantAccountService.getMerchantAccount(company, subdivision)).thenReturn(merchantAccount);
		RefundResponse refundResponse = new RefundResponse();
		when(rawRefundService.refund(any(Refund.class))).thenReturn(refundResponse);

		// @formatter:off
		mockMvc.perform(post(RefundController.PATH + "/raw/" + company).
				secure(true).
				with(testSecurityContext()).
				content(objectMapper.writeValueAsString(refund)).
				param("subdivision", subdivision).
				contentType(MediaType.APPLICATION_JSON).
				accept(MediaType.APPLICATION_JSON)).
			andExpect(status().isOk()).
			andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
		// @formatter:on

		verify(rawRefundService).refund(rawRefundCaptor.capture());
		verifyZeroInteractions(merchantAccountService);
	}

	@Test
	public void testSubmitRefund_json() throws Exception {
		RefundRequest refund = RandomUtils.createInstance(RefundRequest.class);

		when(merchantAccountService.getMerchantAccount(company, subdivision)).thenReturn(merchantAccount);
		au.com.winning.adyen_client.service.dto.RefundResponse refundResponse = RandomUtils
				.createInstance(au.com.winning.adyen_client.service.dto.RefundResponse.class);
		when(refundService.refund(any(RefundRequest.class), eq(merchantAccount))).thenReturn(refundResponse);

		// @formatter:off
		mockMvc.perform(post(RefundController.PATH + "/" + company).
				secure(true).
				with(testSecurityContext()).
				content(objectMapper.writeValueAsString(refund)).
				param("subdivision", subdivision).
				contentType(MediaType.APPLICATION_JSON).
				accept(MediaType.APPLICATION_JSON)).
			andExpect(status().isOk()).
			andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)).
			andExpect(jsonPath("$.received").value(refundResponse.isReceived())).
			andExpect(jsonPath("$.response").value(refundResponse.getResponse()));
		// @formatter:on
	}
}
