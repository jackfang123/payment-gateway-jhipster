package au.com.winning.payment_gateway.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.testSecurityContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.test.web.servlet.MvcResult;

import com.adyen.services.payment.Authorise;
import com.adyen.services.payment.AuthoriseResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

import au.com.winning.adyen_client.service.PaymentService;
import au.com.winning.adyen_client.service.dto.PaymentRequest;
import au.com.winning.adyen_client.service.dto.PaymentResponse;
import au.com.winning.adyen_client.service.dto.PaymentWithRecurringSetupRequest;
import au.com.winning.adyen_client.service.dto.RecurringPaymentRequest;
import au.com.winning.adyen_client.util.MarshallerUtil;
import au.com.winning.payment_gateway.controller.dto.PaymentFormDto;
import au.com.winning.payment_gateway.service.MerchantAccountService;
import au.com.winning.payment_gateway.util.DtoFactory;
import au.com.winning.random_utils.RandomUtils;

public class PaymentControllerTest extends AbstractControllerTest {
	private Logger logger = LoggerFactory.getLogger(PaymentControllerTest.class);

	@Autowired
	private PaymentService paymentService;

	@Autowired
	private au.com.winning.adyen_client.service.raw.PaymentService rawPaymentService;

	@Autowired
	private MerchantAccountService merchantAccountService;

	@Value("${adyen.merchantAccount}")
	private String merchantAccount;

	private String overridenMerchantAccount;

	@Captor
	private ArgumentCaptor<PaymentRequest> requestCaptor;

	@Captor
	private ArgumentCaptor<Authorise> authoriseCaptor;

	@Autowired
	private Marshaller marshaller;

	@Autowired
	private Unmarshaller unmarshaller;

	@Autowired
	@Qualifier("adyenClientObjectMapper")
	private ObjectMapper objectMapper;

	private String company;

	private String subdivision;

	@Captor
	private ArgumentCaptor<PaymentRequest> paymentCaptor;

	@Before
	public void init() {
		company = RandomUtils.createString();
		subdivision = RandomUtils.createString();
		overridenMerchantAccount = RandomUtils.createString();

		reset(rawPaymentService, paymentService, merchantAccountService);
		when(merchantAccountService.getMerchantAccount(company, subdivision)).thenReturn(overridenMerchantAccount);
	}

	@Test
	public void testSubmitTestPaymentPageJson() throws Exception {
		String encryptedAdyenDetails = "whatever";
		BigDecimal amount = new BigDecimal(150);

		PaymentFormDto form = new PaymentFormDto();
		form.setEncryptedAdyenData(encryptedAdyenDetails);
		form.setAmount(amount);

		PaymentResponse paymentResponse = RandomUtils.createInstance(PaymentResponse.class);
		when(paymentService.pay(any(PaymentRequest.class), eq(merchantAccount))).thenReturn(paymentResponse);

		// @formatter:off
		String json = objectMapper.writeValueAsString(form);
		logger.debug(json);
		mockMvc.perform(post(PaymentController.PATH + "/json").
				with(testSecurityContext()).
				content(json).
				contentType(MediaType.APPLICATION_JSON_UTF8).
				secure(true).
				with(csrf())).
			andExpect(status().isOk()).
			andExpect(content().json(objectMapper.writeValueAsString(paymentResponse)));
		// @formatter:on

		verify(paymentService).pay(requestCaptor.capture(), eq(merchantAccount));

		PaymentRequest request = requestCaptor.getValue();
		assertEquals(amount, request.getAmount());
		assertEquals(encryptedAdyenDetails, request.getEncryptedAdyenDetails());
		assertNotNull(request.getReferenceNo());
	}

	@Test
	public void testSubmitRawPayment_xml() throws Exception {
		Authorise authorise = new Authorise();
		authorise.setPaymentRequest(new com.adyen.services.payment.PaymentRequest());

		when(rawPaymentService.pay(any(Authorise.class))).thenReturn(new AuthoriseResponse());

		// @formatter:off
		MvcResult result = mockMvc.perform(post(PaymentController.PATH + "/raw").
				contentType(MediaType.APPLICATION_XML).
				accept(MediaType.APPLICATION_XML).
				content(MarshallerUtil.marshal(marshaller, authorise)).
				secure(true)).
			andExpect(status().isOk()).
			andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_XML)).
			andReturn();
		// @formatter:on

		String xml = result.getResponse().getContentAsString();
		logger.debug(xml);
		AuthoriseResponse response = MarshallerUtil.unmarshal(unmarshaller, xml, AuthoriseResponse.class);
		assertNotNull(response);

		verify(rawPaymentService).pay(authoriseCaptor.capture());
		verifyZeroInteractions(merchantAccountService);
	}

	@Test
	public void testSubmitRawPayment_json() throws Exception {
		Authorise authorise = new Authorise();
		authorise.setPaymentRequest(new com.adyen.services.payment.PaymentRequest());

		when(rawPaymentService.pay(any(Authorise.class))).thenReturn(new AuthoriseResponse());

		// @formatter:off
		MvcResult result = mockMvc.perform(post(PaymentController.PATH + "/raw").
				contentType(MediaType.APPLICATION_JSON).
				accept(MediaType.APPLICATION_JSON).
				with(testSecurityContext()).
				content(objectMapper.writeValueAsString(authorise)).
				secure(true)).
			andExpect(status().isOk()).
			andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)).
			andReturn();
		// @formatter:on

		logger.debug(result.getResponse().getContentAsString());
		verify(rawPaymentService).pay(authoriseCaptor.capture());
		verifyZeroInteractions(merchantAccountService);
	}

	@Test
	public void testSubmitNewPayment_json() throws Exception {
		PaymentRequest request = DtoFactory.createPaymentRequest();

		PaymentResponse paymentResponse = RandomUtils.createInstance(PaymentResponse.class);

		when(paymentService.pay(request, overridenMerchantAccount)).thenReturn(paymentResponse);

		String json = objectMapper.writeValueAsString(request);
		logger.debug(json);

		// @formatter:off		
		mockMvc.perform(post(PaymentController.PATH + "/new/" + company).
				param("subdivision", subdivision).
				contentType(MediaType.APPLICATION_JSON).
				accept(MediaType.APPLICATION_JSON).
				with(testSecurityContext()).
				content(json).
				secure(true)).
			andExpect(status().isOk()).
			andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)).
			andExpect(jsonPath("$.pspReference").value(paymentResponse.getPspReference())).
			andExpect(jsonPath("$.resultCode").value(paymentResponse.getResultCode())).
			andExpect(jsonPath("$.authCode").value(paymentResponse.getAuthCode())).
			andExpect(jsonPath("$.cardBin").value(paymentResponse.getCardBin())).
			andExpect(jsonPath("$.cardSummary").value(paymentResponse.getCardSummary())).
			andExpect(jsonPath("$.cardPaymentMethod").value(paymentResponse.getCardPaymentMethod().toString())).
			andExpect(jsonPath("$.cardIssuingCountry").value(paymentResponse.getCardIssuingCountry())).
			andExpect(jsonPath("$.cardType").value(paymentResponse.getCardType().toString()));
		// @formatter:on
	}

	@Test
	public void testSubmitNewPayment_json_invalidRequest() throws Exception {
		PaymentRequest request = DtoFactory.createPaymentRequest();
		request.setEncryptedAdyenDetails(null);

		// @formatter:off
		mockMvc.perform(post(PaymentController.PATH + "/new/" + company).
				param("subdivision", subdivision).
				contentType(MediaType.APPLICATION_JSON).
				accept(MediaType.APPLICATION_JSON).
				with(testSecurityContext()).
				content(objectMapper.writeValueAsString(request)).
				secure(true)).
			andExpect(status().isBadRequest()).
			andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)).
			andExpect(content().string(containsString("encryptedAdyenDetails")));
		// @formatter:on

		verifyZeroInteractions(paymentService);
	}

	@Test
	public void testSubmitNewPaymentWithRecurringSetup_json() throws Exception {
		PaymentWithRecurringSetupRequest request = DtoFactory.createPaymentRequestWithRecurringSetupRequest();

		PaymentResponse paymentResponse = RandomUtils.createInstance(PaymentResponse.class);

		when(paymentService.payWithRecurringSetup(request, overridenMerchantAccount)).thenReturn(paymentResponse);

		// @formatter:off
		mockMvc.perform(post(PaymentController.PATH + "/newWithRecurringSetup/" + company).
				param("subdivision", subdivision).
				contentType(MediaType.APPLICATION_JSON).
				accept(MediaType.APPLICATION_JSON).
				with(testSecurityContext()).
				content(objectMapper.writeValueAsString(request)).
				secure(true)).
			andExpect(status().isOk()).
			andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)).
			andExpect(jsonPath("$.pspReference").value(paymentResponse.getPspReference())).
			andExpect(jsonPath("$.resultCode").value(paymentResponse.getResultCode())).
			andExpect(jsonPath("$.authCode").value(paymentResponse.getAuthCode())).
			andExpect(jsonPath("$.cardBin").value(paymentResponse.getCardBin())).
			andExpect(jsonPath("$.cardSummary").value(paymentResponse.getCardSummary())).
			andExpect(jsonPath("$.cardPaymentMethod").value(paymentResponse.getCardPaymentMethod().toString())).
			andExpect(jsonPath("$.cardIssuingCountry").value(paymentResponse.getCardIssuingCountry())).
			andExpect(jsonPath("$.cardType").value(paymentResponse.getCardType().toString()));
		// @formatter:on
	}

	@Test
	public void testSubmitNewPaymentWithRecurringSetup_json_invalidRequest() throws Exception {
		PaymentWithRecurringSetupRequest request = DtoFactory.createPaymentRequestWithRecurringSetupRequest();

		request.setShopperReference(null);

		// @formatter:off
		mockMvc.perform(post(PaymentController.PATH + "/newWithRecurringSetup/" + company).
				param("subdivision", subdivision).
				contentType(MediaType.APPLICATION_JSON).
				accept(MediaType.APPLICATION_JSON).
				with(testSecurityContext()).
				content(objectMapper.writeValueAsString(request)).
				secure(true)).
			andExpect(status().isBadRequest()).
			andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)).
			andExpect(content().string(containsString("shopperReference")));
		// @formatter:on

		verifyZeroInteractions(paymentService);
	}

	@Test
	public void testSubmitRecurringPayment_json() throws Exception {
		RecurringPaymentRequest request = DtoFactory.createRecurringPaymentRequest();

		PaymentResponse paymentResponse = RandomUtils.createInstance(PaymentResponse.class);

		when(paymentService.payRecurring(request, overridenMerchantAccount)).thenReturn(paymentResponse);

		// @formatter:off
		mockMvc.perform(post(PaymentController.PATH + "/recurring/" + company).
				param("subdivision", subdivision).
				contentType(MediaType.APPLICATION_JSON).
				accept(MediaType.APPLICATION_JSON).
				with(testSecurityContext()).
				content(objectMapper.writeValueAsString(request)).
				secure(true)).
			andExpect(status().isOk()).
			andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)).
			andExpect(jsonPath("$.pspReference").value(paymentResponse.getPspReference())).
			andExpect(jsonPath("$.resultCode").value(paymentResponse.getResultCode())).
			andExpect(jsonPath("$.authCode").value(paymentResponse.getAuthCode())).
			andExpect(jsonPath("$.cardBin").value(paymentResponse.getCardBin())).
			andExpect(jsonPath("$.cardSummary").value(paymentResponse.getCardSummary())).
			andExpect(jsonPath("$.cardPaymentMethod").value(paymentResponse.getCardPaymentMethod().toString())).
			andExpect(jsonPath("$.cardIssuingCountry").value(paymentResponse.getCardIssuingCountry())).
			andExpect(jsonPath("$.cardType").value(paymentResponse.getCardType().toString()));
		// @formatter:on
	}

	@Test
	public void testSubmitRecurringPayment_json_invalidRequest() throws Exception {
		RecurringPaymentRequest request = DtoFactory.createRecurringPaymentRequest();
		request.setReferenceNo(null);

		// @formatter:off
		mockMvc.perform(post(PaymentController.PATH + "/recurring/" + company).
				param("subdivision", subdivision).
				contentType(MediaType.APPLICATION_JSON).
				accept(MediaType.APPLICATION_JSON).
				with(testSecurityContext()).
				content(objectMapper.writeValueAsString(request)).
				secure(true)).
			andExpect(status().isBadRequest()).
			andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)).
			andExpect(content().string(containsString("referenceNo")));
		// @formatter:on
	}
}
