package au.com.winning.payment_gateway.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.testSecurityContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.databind.ObjectMapper;

import au.com.winning.payment_gateway.controller.dto.TenderRequestDto;
import au.com.winning.payment_gateway.service.WebSocketClientService;
import au.com.winning.payment_gateway.util.DtoFactory;
import au.com.winning.random_utils.RandomUtils;

public class PosControllerTest extends AbstractControllerTest {

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private WebSocketClientService service;

	@Test
	public void testCreateTender() throws Exception {
		String id = RandomUtils.createString();
		TenderRequestDto tender = DtoFactory.createTenderRequestDto();

		// @formatter:off
		mockMvc.perform(post(PosController.PATH + "/createTender/{id}", id)
				.secure(true)
				.with(testSecurityContext())
				.with(csrf())
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsString(tender)))
			.andExpect(status().isOk());
		// @formatter:on

	}
}
