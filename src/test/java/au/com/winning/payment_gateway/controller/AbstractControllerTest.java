package au.com.winning.payment_gateway.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;

import javax.servlet.Filter;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import au.com.winning.payment_gateway.config.ControllerTestConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ControllerTestConfig.class)
@WebAppConfiguration
@ActiveProfiles("unit")
public abstract class AbstractControllerTest {

	@Autowired
	protected WebApplicationContext wac;

	@Autowired
	protected Filter springSecurityFilterChain;
	protected MockMvc mockMvc;

	public AbstractControllerTest() {
		super();
	}

	@Before
	public void beforeTest() {
		mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).addFilter(springSecurityFilterChain).alwaysDo(log()).build();
	}

}