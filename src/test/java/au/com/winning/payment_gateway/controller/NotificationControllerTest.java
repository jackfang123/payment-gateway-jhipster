package au.com.winning.payment_gateway.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.testSecurityContext;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import au.com.winning.payment_gateway.domain.enumeration.NotificationType;
import au.com.winning.payment_gateway.security.AuthoritiesConstants;

@WithMockUser(authorities = AuthoritiesConstants.NOTIFICATION)
public class NotificationControllerTest extends AbstractControllerTest {

	@Test
	public void testStandardReceive() throws Exception {
		// @formatter:off
		mockMvc.perform(post(NotificationController.PATH + "/" + NotificationType.STANDARD.name()).
				content("hello").contentType(MediaType.APPLICATION_JSON_UTF8).
				with(testSecurityContext()).
				secure(true)).
			andExpect(status().isOk()).
			andExpect(content().string("[accepted]")).
			andExpect(authenticated().withRoles(AuthoritiesConstants.asRole(AuthoritiesConstants.NOTIFICATION)));
		// @formatter:on
	}

	@WithMockUser
	@Test
	public void testStandardReceive_noAuth() throws Exception {
		// @formatter:off
		mockMvc.perform(post(NotificationController.PATH + "/" + NotificationType.STANDARD.name()).
				content("hello").contentType(MediaType.APPLICATION_JSON_UTF8).
				with(testSecurityContext()).
				secure(true)).
			andExpect(status().isForbidden());
		// @formatter:on
	}
}
