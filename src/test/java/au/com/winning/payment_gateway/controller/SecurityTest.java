package au.com.winning.payment_gateway.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.testSecurityContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

public class SecurityTest extends AbstractControllerTest {

	@Test
	public void testInsecureHttpAccess() throws Exception {
		// @formatter:off
		mockMvc.perform(get("/anyPath").with(testSecurityContext())).
			andExpect(status().is3xxRedirection()).
			andExpect(redirectedUrl("https://localhost/anyPath"));
		// @formatter:on
	}

	@Test
	public void testNoCredentials() throws Exception {
		// @formatter:off
		mockMvc.perform(get("/anyPath").with(testSecurityContext()).secure(true)).
			andExpect(status().isUnauthorized());
		// @formatter:on
	}

	@Test
	public void testStaticPage() throws Exception {
		// @formatter:off
		mockMvc.perform(get("/").with(testSecurityContext()).secure(true)).
			andExpect(status().isOk());
		// @formatter:on
	}

	@Test
	public void testFavIcon() throws Exception {
		// need to reconstruct this to prevent logging out of binary data
		mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).addFilter(springSecurityFilterChain).build();
		// @formatter:off
		mockMvc.perform(get("/favicon.ico").with(testSecurityContext()).secure(true)).
			andExpect(status().isOk());
		// @formatter:on
	}

}
