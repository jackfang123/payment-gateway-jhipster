package au.com.winning.payment_gateway.assembler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;

import au.com.winning.payment_gateway.domain.Event;
import au.com.winning.payment_gateway.domain.Notification;
import au.com.winning.payment_gateway.domain.enumeration.NotificationType;
import au.com.winning.random_utils.RandomUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class EventAssemblerTest {
	@Configuration
	@ComponentScan(basePackageClasses = EventAssembler.class)
	public static class Config {
		@Bean
		public ObjectMapper objectMapper() {
			return new ObjectMapper();
		}
	}

	@Autowired
	private EventAssembler assembler;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	public void testAssemble() throws IOException {
		NotificationType type = (NotificationType) RandomUtils.getArrayElement(NotificationType.values());
		String json = IOUtils.toString(new ClassPathResource("json/refund_event.json").getInputStream(), "UTF8");
		Event event = assembler.assemble(type, json);

		assertNotNull(event.getCreated());
		assertEquals(json, event.getPayload());
		assertEquals(type, event.getNotificationType());

		List<Map<String, Object>> notificationJsons = JsonPath.read(json, "$.notificationItems[*].NotificationRequestItem");
		assertEquals(event.getNotifications().size(), notificationJsons.size());

		for (int i = 0; i < notificationJsons.size(); i++) {
			Map<String, Object> notificationJson = notificationJsons.get(i);
			Notification not = event.getNotifications().get(i);
			Map map = objectMapper.readValue(not.getJson(), Map.class);
			assertEquals(notificationJson, map);
		}
	}
}
