package au.com.winning.payment_gateway.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import au.com.winning.payment_gateway.domain.Domain;
import au.com.winning.payment_gateway.security.SpringSecurityAuditorAware;

@Configuration
@Import({ DatabaseConfiguration.class, AsyncConfiguration.class })
@EnableAutoConfiguration
@EntityScan(basePackageClasses = Domain.class)
@EnableConfigurationProperties({ JHipsterProperties.class, LiquibaseProperties.class })
public class RepositoryTestConfig {
	@Bean
	public SpringSecurityAuditorAware springSecurityAuditorAware() {
		return new SpringSecurityAuditorAware();
	}
}
