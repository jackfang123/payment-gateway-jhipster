package au.com.winning.payment_gateway.config;

import static org.mockito.Mockito.mock;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import au.com.winning.payment_gateway.assembler.EventAssembler;

@Configuration
public class MockedAssemblerTestConfig {
	@Bean
	public EventAssembler eventAssembler() {
		return mock(EventAssembler.class);
	}
}
