package au.com.winning.payment_gateway.config;

import static org.mockito.Mockito.mock;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import au.com.winning.payment_gateway.repository.EventRepository;
import au.com.winning.payment_gateway.repository.PersistentTokenRepository;
import au.com.winning.payment_gateway.repository.UserRepository;

@Configuration
public class MockedDaosTestConfig {
	@Bean
	public EventRepository eventDao() {
		return mock(EventRepository.class);
	}

	@Bean
	public UserRepository userDao() {
		return mock(UserRepository.class);
	}

	@Bean
	public PersistentTokenRepository persistentTokenDao() {
		return mock(PersistentTokenRepository.class);
	}
}
