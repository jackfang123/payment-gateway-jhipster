package au.com.winning.payment_gateway.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import au.com.winning.adyen_client.config.DozerConfig;
import au.com.winning.adyen_client.config.JacksonConfig;
import au.com.winning.adyen_client.config.MarshallerConfig;
import au.com.winning.payment_gateway.controller.Controller;
import au.com.winning.payment_gateway.security.UserDetailsService;

@Configuration
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class })
@ComponentScan(basePackageClasses = { Controller.class, UserDetailsService.class })
@Import({ MarshallerConfig.class, DozerConfig.class, JacksonConfig.class, MockedServicesTestConfig.class, MockedAssemblerTestConfig.class,
		MockedDaosTestConfig.class, SecurityConfiguration.class, JHipsterProperties.class })
public class ControllerTestConfig {

}