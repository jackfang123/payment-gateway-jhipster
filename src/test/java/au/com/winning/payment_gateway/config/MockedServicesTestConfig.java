package au.com.winning.payment_gateway.config;

import static org.mockito.Mockito.mock;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import au.com.winning.adyen_client.service.PaymentService;
import au.com.winning.adyen_client.service.raw.RefundService;
import au.com.winning.payment_gateway.service.MerchantAccountService;
import au.com.winning.payment_gateway.service.PedService;
import au.com.winning.payment_gateway.service.WebSocketClientService;

@Configuration
public class MockedServicesTestConfig {
	@Bean
	public PaymentService paymentService() {
		return mock(PaymentService.class);
	}

	@Bean
	public au.com.winning.adyen_client.service.raw.PaymentService rawPaymentService() {
		return mock(au.com.winning.adyen_client.service.raw.PaymentService.class);
	}

	@Bean
	public MerchantAccountService merchantAccountService() {
		return mock(MerchantAccountService.class);
	}

	@Bean
	public RefundService rawRefundService() {
		return mock(RefundService.class);
	}

	@Bean
	public au.com.winning.adyen_client.service.RefundService refundService() {
		return mock(au.com.winning.adyen_client.service.RefundService.class);
	}

	@Bean
	public WebSocketClientService webSocketClientService() {
		return mock(WebSocketClientService.class);
	}

	@Bean
	public PedService pedService() {
		return mock(PedService.class);
	}
}
