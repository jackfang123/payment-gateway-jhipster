package au.com.winning.payment_gateway.util;

import au.com.winning.adyen_client.service.dto.PaymentRequest;
import au.com.winning.adyen_client.service.dto.PaymentWithRecurringSetupRequest;
import au.com.winning.adyen_client.service.dto.RecurringPaymentRequest;
import au.com.winning.adyen_client.service.dto.RefundRequest;
import au.com.winning.adyen_client.service.dto.fraud.Address;
import au.com.winning.adyen_client.service.dto.fraud.Contact;
import au.com.winning.adyen_client.service.dto.fraud.FraudCheckingDetails;
import au.com.winning.adyen_client.service.dto.fraud.Line;
import au.com.winning.payment_gateway.controller.dto.TenderRequestDto;
import au.com.winning.pos_dto.PedLibraryCallbackDto;
import au.com.winning.random_utils.RandomUtils;

public class DtoFactory {

	public static RefundRequest createRefundRequest() {
		return RandomUtils.createInstance(RefundRequest.class);
	}

	public static PaymentRequest createPaymentRequest() {
		PaymentRequest request = RandomUtils.createInstance(PaymentRequest.class);
		request.setFraudCheckingDetails(createFraudCheckingDetails());
		return request;
	}

	public static FraudCheckingDetails createFraudCheckingDetails() {
		FraudCheckingDetails det = RandomUtils.createInstance(FraudCheckingDetails.class);
		det.setBillingContact(createContact());
		det.getLines().add(createLine());
		det.setShippingContact(createContact());
		return det;
	}

	public static Line createLine() {
		return RandomUtils.createInstance(Line.class);
	}

	public static Contact createContact() {
		Contact contact = RandomUtils.createInstance(Contact.class);
		contact.setEmail(RandomUtils.createEmail());
		contact.setAddress(createAddress());
		return contact;
	}

	public static Address createAddress() {
		Address addr = RandomUtils.createInstance(Address.class);
		addr.setCountry("AU");
		addr.setPostcode("2000");
		return addr;
	}

	public static PaymentWithRecurringSetupRequest createPaymentRequestWithRecurringSetupRequest() {
		PaymentWithRecurringSetupRequest request = RandomUtils.createInstance(PaymentWithRecurringSetupRequest.class);
		request.setShopperEmail(RandomUtils.createEmail());
		request.setFraudCheckingDetails(createFraudCheckingDetails());
		return request;
	}

	public static RecurringPaymentRequest createRecurringPaymentRequest() {
		RecurringPaymentRequest request = RandomUtils.createInstance(RecurringPaymentRequest.class);
		request.setShopperEmail(RandomUtils.createEmail());
		request.setFraudCheckingDetails(createFraudCheckingDetails());
		return request;
	}

	public static TenderRequestDto createTenderRequestDto() {
		return RandomUtils.createInstance(TenderRequestDto.class);
	}

	public static PedLibraryCallbackDto createPedLibraryCallbackDto() {
		return RandomUtils.createInstance(PedLibraryCallbackDto.class);
	}

}
