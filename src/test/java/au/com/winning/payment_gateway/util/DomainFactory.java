package au.com.winning.payment_gateway.util;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;

import au.com.winning.payment_gateway.domain.Event;
import au.com.winning.payment_gateway.domain.Notification;
import au.com.winning.random_utils.RandomUtils;

public class DomainFactory {

	public static Event createEvent() {
		Event event = RandomUtils.createInstance(Event.class);
		event.setId(null);
		try {
			event.setPayload(IOUtils.toString(new ClassPathResource("json/refund_event.json").getInputStream()));
		} catch (IOException e) {
			throw new RuntimeException("couldn't read json file", e);
		}
		event.setCreated(event.getCreated().withNano(0));
		event.addNotifications(createNotification());
		return event;
	}

	private static Notification createNotification() {
		Notification not = RandomUtils.createInstance(Notification.class);
		not.setId(null);
		try {
			not.setJson(IOUtils.toString(new ClassPathResource("json/refund_notification.json").getInputStream()));
		} catch (IOException e) {
			throw new RuntimeException("couldn't read json file", e);
		}
		return not;
	}

	// public static User createUser() {
	// User user = RandomUtils.createInstance(User.class);
	// for (UserRoleEnum roleEnum : UserRoleEnum.values()) {
	// UserRole role = new UserRole();
	// role.setRole(roleEnum);
	// user.getRoles().add(role);
	// }
	// return user;
	// }
}
