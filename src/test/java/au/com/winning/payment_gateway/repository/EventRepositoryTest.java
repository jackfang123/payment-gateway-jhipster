package au.com.winning.payment_gateway.repository;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import au.com.winning.payment_gateway.domain.Event;
import au.com.winning.payment_gateway.util.DomainFactory;

public class EventRepositoryTest extends AbstractRepositoryTest {

	@Autowired
	private EventRepository eventDao;

	@Test
	public void testSaveAndLoad() {
		Event event = DomainFactory.createEvent();
		event = eventDao.save(event);

		assertEquals(event, eventDao.findOne(event.getId()));
	}
}
