package au.com.winning.payment_gateway.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;

import au.com.winning.payment_gateway.PaymentGatewayApp;
import au.com.winning.payment_gateway.domain.Event;
import au.com.winning.payment_gateway.domain.enumeration.NotificationType;
import au.com.winning.payment_gateway.repository.EventRepository;

/**
 * Test class for the EventResource REST controller.
 *
 * @see EventResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PaymentGatewayApp.class)
public class EventResourceIntTest {

	private static final ZonedDateTime DEFAULT_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
	private static final ZonedDateTime UPDATED_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

	private static final NotificationType DEFAULT_NOTIFICATION_TYPE = NotificationType.STANDARD;
	private static final NotificationType UPDATED_NOTIFICATION_TYPE = NotificationType.STANDARD;

	private static final String DEFAULT_PAYLOAD = "[1]";
	private static final String UPDATED_PAYLOAD = "[2]";

	@Inject
	private EventRepository eventRepository;

	@Inject
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Inject
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Inject
	private EntityManager em;

	@Autowired
	private ObjectMapper objectMapper;

	private MockMvc restEventMockMvc;

	private Event event;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		EventResource eventResource = new EventResource();
		ReflectionTestUtils.setField(eventResource, "eventRepository", eventRepository);
		this.restEventMockMvc = MockMvcBuilders.standaloneSetup(eventResource).setCustomArgumentResolvers(pageableArgumentResolver)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it,
	 * if they test an entity which requires the current entity.
	 */
	public static Event createEntity(EntityManager em) {
		Event event = new Event().created(DEFAULT_CREATED).notificationType(DEFAULT_NOTIFICATION_TYPE).payload(DEFAULT_PAYLOAD);
		return event;
	}

	@Before
	public void initTest() {
		event = createEntity(em);
	}

	@Test
	@Transactional
	public void createEvent() throws Exception {
		int databaseSizeBeforeCreate = eventRepository.findAll().size();

		// Create the Event

		restEventMockMvc.perform(post("/api/events").contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(event)))
				.andExpect(status().isCreated());

		// Validate the Event in the database
		List<Event> eventList = eventRepository.findAll();
		assertThat(eventList).hasSize(databaseSizeBeforeCreate + 1);
		Event testEvent = eventList.get(eventList.size() - 1);
		assertThat(testEvent.getCreated()).isEqualTo(DEFAULT_CREATED);
		assertThat(testEvent.getNotificationType()).isEqualTo(DEFAULT_NOTIFICATION_TYPE);
		assertThat(testEvent.getPayload()).isEqualTo(DEFAULT_PAYLOAD);
	}

	@Test
	@Transactional
	public void createEventWithExistingId() throws Exception {
		int databaseSizeBeforeCreate = eventRepository.findAll().size();

		// Create the Event with an existing ID
		Event existingEvent = new Event();
		existingEvent.setId(1L);

		// An entity with an existing ID cannot be created, so this API call must fail
		restEventMockMvc.perform(post("/api/events").contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(existingEvent)))
				.andExpect(status().isBadRequest());

		// Validate the Alice in the database
		List<Event> eventList = eventRepository.findAll();
		assertThat(eventList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void checkCreatedIsRequired() throws Exception {
		int databaseSizeBeforeTest = eventRepository.findAll().size();
		// set the field null
		event.setCreated(null);

		// Create the Event, which fails.

		restEventMockMvc.perform(post("/api/events").contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(event)))
				.andExpect(status().isBadRequest());

		List<Event> eventList = eventRepository.findAll();
		assertThat(eventList).hasSize(databaseSizeBeforeTest);
	}

	@Test
	@Transactional
	public void checkNotificationTypeIsRequired() throws Exception {
		int databaseSizeBeforeTest = eventRepository.findAll().size();
		// set the field null
		event.setNotificationType(null);

		// Create the Event, which fails.

		restEventMockMvc.perform(post("/api/events").contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(event)))
				.andExpect(status().isBadRequest());

		List<Event> eventList = eventRepository.findAll();
		assertThat(eventList).hasSize(databaseSizeBeforeTest);
	}

	@Test
	@Transactional
	public void checkPayloadIsRequired() throws Exception {
		int databaseSizeBeforeTest = eventRepository.findAll().size();
		// set the field null
		event.setPayload(null);

		// Create the Event, which fails.

		restEventMockMvc.perform(post("/api/events").contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(event)))
				.andExpect(status().isBadRequest());

		List<Event> eventList = eventRepository.findAll();
		assertThat(eventList).hasSize(databaseSizeBeforeTest);
	}

	@Test
	@Transactional
	public void getAllEvents() throws Exception {
		// Initialize the database
		eventRepository.saveAndFlush(event);

		// Get all the eventList
		restEventMockMvc.perform(get("/api/events?sort=id,desc")).andExpect(status().isOk()).andDo(log())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(event.getId().intValue())))
				// .andExpect(jsonPath("$.[*].created").value(objectMapper.writeValueAsString(DEFAULT_CREATED)))
				.andExpect(jsonPath("$.[*].notificationType").value(hasItem(DEFAULT_NOTIFICATION_TYPE.toString())))
				.andExpect(jsonPath("$.[*].payload").value(hasItem(DEFAULT_PAYLOAD.toString())));
	}

	@Test
	@Transactional
	public void getEvent() throws Exception {
		// Initialize the database
		eventRepository.saveAndFlush(event);

		objectMapper.writeValueAsString(event);

		// Get the event
		restEventMockMvc.perform(get("/api/events/{id}", event.getId())).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(jsonPath("$.id").value(event.getId().intValue()))
				.andExpect(jsonPath("$.created").value(objectMapper.writeValueAsString(DEFAULT_CREATED)))
				.andExpect(jsonPath("$.notificationType").value(DEFAULT_NOTIFICATION_TYPE.toString()))
				.andExpect(jsonPath("$.payload").value(DEFAULT_PAYLOAD.toString()));
	}

	@Test
	@Transactional
	public void getNonExistingEvent() throws Exception {
		// Get the event
		restEventMockMvc.perform(get("/api/events/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void updateEvent() throws Exception {
		// Initialize the database
		eventRepository.saveAndFlush(event);
		int databaseSizeBeforeUpdate = eventRepository.findAll().size();

		// Update the event
		Event updatedEvent = eventRepository.findOne(event.getId());
		updatedEvent.created(UPDATED_CREATED).notificationType(UPDATED_NOTIFICATION_TYPE).payload(UPDATED_PAYLOAD);

		restEventMockMvc.perform(put("/api/events").contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(updatedEvent)))
				.andExpect(status().isOk());

		// Validate the Event in the database
		List<Event> eventList = eventRepository.findAll();
		assertThat(eventList).hasSize(databaseSizeBeforeUpdate);
		Event testEvent = eventList.get(eventList.size() - 1);
		assertThat(testEvent.getCreated()).isEqualTo(UPDATED_CREATED);
		assertThat(testEvent.getNotificationType()).isEqualTo(UPDATED_NOTIFICATION_TYPE);
		assertThat(testEvent.getPayload()).isEqualTo(UPDATED_PAYLOAD);
	}

	@Test
	@Transactional
	public void updateNonExistingEvent() throws Exception {
		int databaseSizeBeforeUpdate = eventRepository.findAll().size();

		// Create the Event

		// If the entity doesn't have an ID, it will be created instead of just being updated
		restEventMockMvc.perform(put("/api/events").contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(event)))
				.andExpect(status().isCreated());

		// Validate the Event in the database
		List<Event> eventList = eventRepository.findAll();
		assertThat(eventList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteEvent() throws Exception {
		// Initialize the database
		eventRepository.saveAndFlush(event);
		int databaseSizeBeforeDelete = eventRepository.findAll().size();

		// Get the event
		restEventMockMvc.perform(delete("/api/events/{id}", event.getId()).accept(TestUtil.APPLICATION_JSON_UTF8)).andExpect(status().isOk());

		// Validate the database is empty
		List<Event> eventList = eventRepository.findAll();
		assertThat(eventList).hasSize(databaseSizeBeforeDelete - 1);
	}
}
