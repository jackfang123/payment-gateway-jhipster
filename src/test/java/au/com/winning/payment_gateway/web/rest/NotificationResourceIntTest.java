package au.com.winning.payment_gateway.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;

import au.com.winning.payment_gateway.PaymentGatewayApp;
import au.com.winning.payment_gateway.domain.Event;
import au.com.winning.payment_gateway.domain.Notification;
import au.com.winning.payment_gateway.repository.NotificationRepository;

/**
 * Test class for the NotificationResource REST controller.
 *
 * @see NotificationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PaymentGatewayApp.class)
public class NotificationResourceIntTest {

	private static final String DEFAULT_JSON = "[1]";
	private static final String UPDATED_JSON = "[2]";

	@Inject
	private NotificationRepository notificationRepository;

	@Inject
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Inject
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Inject
	private EntityManager em;

	private MockMvc restNotificationMockMvc;

	private Notification notification;

	@Autowired
	private ObjectMapper mapper;

	private Logger logger = LoggerFactory.getLogger(NotificationResourceIntTest.class);

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		NotificationResource notificationResource = new NotificationResource();
		ReflectionTestUtils.setField(notificationResource, "notificationRepository", notificationRepository);
		this.restNotificationMockMvc = MockMvcBuilders.standaloneSetup(notificationResource).setCustomArgumentResolvers(pageableArgumentResolver)
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it,
	 * if they test an entity which requires the current entity.
	 */
	public static Notification createEntity(EntityManager em) {
		Notification notification = new Notification().json(DEFAULT_JSON);
		// Add required entity
		Event event = EventResourceIntTest.createEntity(em);
		em.persist(event);
		em.flush();
		notification.setEvent(event);
		return notification;
	}

	@Before
	public void initTest() {
		notification = createEntity(em);
	}

	@Test
	@Transactional
	public void createNotification() throws Exception {
		int databaseSizeBeforeCreate = notificationRepository.findAll().size();

		logger.debug(mapper.writeValueAsString(notification));

		// Create the Notification

		restNotificationMockMvc
				.perform(post("/api/notifications").contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(notification)))
				.andExpect(status().isCreated());

		// Validate the Notification in the database
		List<Notification> notificationList = notificationRepository.findAll();
		assertThat(notificationList).hasSize(databaseSizeBeforeCreate + 1);
		Notification testNotification = notificationList.get(notificationList.size() - 1);
		assertThat(testNotification.getJson()).isEqualTo(DEFAULT_JSON);
	}

	@Test
	@Transactional
	public void createNotificationWithExistingId() throws Exception {
		int databaseSizeBeforeCreate = notificationRepository.findAll().size();

		// Create the Notification with an existing ID
		Notification existingNotification = new Notification();
		existingNotification.setId(1L);

		// An entity with an existing ID cannot be created, so this API call must fail
		restNotificationMockMvc
				.perform(
						post("/api/notifications").contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(existingNotification)))
				.andExpect(status().isBadRequest());

		// Validate the Alice in the database
		List<Notification> notificationList = notificationRepository.findAll();
		assertThat(notificationList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void checkJsonIsRequired() throws Exception {
		int databaseSizeBeforeTest = notificationRepository.findAll().size();
		// set the field null
		notification.setJson(null);

		// Create the Notification, which fails.

		restNotificationMockMvc
				.perform(post("/api/notifications").contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(notification)))
				.andExpect(status().isBadRequest());

		List<Notification> notificationList = notificationRepository.findAll();
		assertThat(notificationList).hasSize(databaseSizeBeforeTest);
	}

	@Test
	@Transactional
	public void getAllNotifications() throws Exception {
		// Initialize the database
		notificationRepository.saveAndFlush(notification);

		// Get all the notificationList
		restNotificationMockMvc.perform(get("/api/notifications?sort=id,desc")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(notification.getId().intValue())))
				.andExpect(jsonPath("$.[*].json").value(hasItem(DEFAULT_JSON.toString())));
	}

	@Test
	@Transactional
	public void getNotification() throws Exception {
		// Initialize the database
		notificationRepository.saveAndFlush(notification);

		// Get the notification
		restNotificationMockMvc.perform(get("/api/notifications/{id}", notification.getId())).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(jsonPath("$.id").value(notification.getId().intValue()))
				.andExpect(jsonPath("$.json").value(DEFAULT_JSON.toString()));
	}

	@Test
	@Transactional
	public void getNonExistingNotification() throws Exception {
		// Get the notification
		restNotificationMockMvc.perform(get("/api/notifications/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void updateNotification() throws Exception {
		// Initialize the database
		notificationRepository.saveAndFlush(notification);
		int databaseSizeBeforeUpdate = notificationRepository.findAll().size();

		// Update the notification
		Notification updatedNotification = notificationRepository.findOne(notification.getId());
		updatedNotification.json(UPDATED_JSON);

		restNotificationMockMvc
				.perform(put("/api/notifications").contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(updatedNotification)))
				.andExpect(status().isOk());

		// Validate the Notification in the database
		List<Notification> notificationList = notificationRepository.findAll();
		assertThat(notificationList).hasSize(databaseSizeBeforeUpdate);
		Notification testNotification = notificationList.get(notificationList.size() - 1);
		assertThat(testNotification.getJson()).isEqualTo(UPDATED_JSON);
	}

	@Test
	@Transactional
	public void updateNonExistingNotification() throws Exception {
		int databaseSizeBeforeUpdate = notificationRepository.findAll().size();

		// Create the Notification

		// If the entity doesn't have an ID, it will be created instead of just being updated
		restNotificationMockMvc
				.perform(put("/api/notifications").contentType(TestUtil.APPLICATION_JSON_UTF8).content(TestUtil.convertObjectToJsonBytes(notification)))
				.andExpect(status().isCreated());

		// Validate the Notification in the database
		List<Notification> notificationList = notificationRepository.findAll();
		assertThat(notificationList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deleteNotification() throws Exception {
		// Initialize the database
		notificationRepository.saveAndFlush(notification);
		int databaseSizeBeforeDelete = notificationRepository.findAll().size();

		// Get the notification
		restNotificationMockMvc.perform(delete("/api/notifications/{id}", notification.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<Notification> notificationList = notificationRepository.findAll();
		assertThat(notificationList).hasSize(databaseSizeBeforeDelete - 1);
	}
}
